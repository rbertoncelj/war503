# War503

Root deployment for WildFly that returns 503 HTTP status for all requests. Reverse proxies (such as nginx or apache) can
use this to detect whether some non-root deployment (an application that you actually care about) is deployed or not.

__Download: [war503-1.0.2.war](http://central.maven.org/maven2/com/bertoncelj/war503/1.0.2/war503-1.0.2.war)__

## Example nginx configuration
```
upstream myapp {
    server app1.example.com:8080;
    server app2.example.com:8080;
}

server {
    listen       80;
    server_name  localhost;

    location / {
        proxy_pass http://myapp/;
        proxy_next_upstream error timeout invalid_header http_503;
    }
}

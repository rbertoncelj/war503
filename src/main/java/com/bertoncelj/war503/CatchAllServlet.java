package com.bertoncelj.war503;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * @author Rok Bertoncelj
 */
@WebServlet("/*")
public class CatchAllServlet extends HttpServlet {
    private Logger logger = Logger.getLogger(CatchAllServlet.class.getName());

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.fine("Handling request to " + req.getRequestURI());
        resp.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "Application not deployed");
    }
}

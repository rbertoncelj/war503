package com.bertoncelj.war503;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;

import static org.junit.Assert.assertEquals;

/**
 * @author Rok Bertoncelj
 */
@SuppressWarnings("ArquillianTooManyDeployment")
@RunWith(Arquillian.class)
@RunAsClient
public class CatchAllServletIT {

    @Deployment(name = "rootApp", testable = false)
    public static WebArchive getRootDeployment() {
        try {
            return ShrinkWrap.create(WebArchive.class)
                    .addClass(CatchAllServlet.class)
                    .addAsWebInfResource(new File("src/main/webapp/WEB-INF/jboss-web.xml"));
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Deployment(name = "nonRootApp", testable = false)
    public static WebArchive getNonRootDeployment() {
        try {
            WebArchive webArchive = ShrinkWrap.create(WebArchive.class)
                    .addClass(FooBarServlet.class)
                    .addAsWebInfResource("foobar-jboss-web.xml", "jboss-web.xml");
            System.out.println(webArchive.toString(true));
            return webArchive;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Test
    public void testStuff() throws Exception {
        URL url = new URL("http://localhost:8080");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        assertEquals(HttpServletResponse.SC_SERVICE_UNAVAILABLE, conn.getResponseCode());

        url = new URL("http://localhost:8080/this-should-fail");
        conn = (HttpURLConnection) url.openConnection();
        assertEquals(HttpServletResponse.SC_SERVICE_UNAVAILABLE, conn.getResponseCode());


        url = new URL("http://localhost:8080/foobar");
        conn = (HttpURLConnection) url.openConnection();
        assertEquals(HttpServletResponse.SC_OK, conn.getResponseCode());

        url = new URL("http://localhost:8080/foobar/something-else");
        conn = (HttpURLConnection) url.openConnection();
        assertEquals(HttpServletResponse.SC_OK, conn.getResponseCode());
    }

}
package com.bertoncelj.war503;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Rok Bertoncelj
 */
@WebServlet("/*")
public class FooBarServlet extends HttpServlet {
    public static final String MESSAGE = "This is FooBar!";

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setStatus(HttpServletResponse.SC_OK);
        resp.getOutputStream().println(MESSAGE);
    }
}
